%global project			Oggify
%global commit0			4412e37b3fb1d574b2af34c0d273f2a9d4989c98
%global shortcommit0	%(c=%{commit0}; echo ${c:0:7})
%global commit1			9ef699f015db815bfcf0f50c5af7791cd70b7f12
%global shortcommit1	%(c=%{commit1}; echo ${c:0:7})
%global checkout		20150615git%shortcommit0

Summary:	Audio conversion tool for music library conversion
Name:		oggify
Version:	2.0.7
Release:	5.%{checkout}%{?dist}
Source0:	http://github.com/spr/%{project}/archive/%{commit0}/%{project}-%{commit0}.tar.gz
Source1:	http://github.com/spr/tag_wrapper/archive/%{commit1}/tag_wrapper-%{commit1}.tar.gz#/%{project}-tag_wrapper-%{commit1}.tar.gz
License:	GPLv2+
BuildArch:	noarch
Url:		http://scottr.org/oggify/
BuildRequires:	python2-devel, python-mutagen
Requires:	flac, vorbis-tools, python-mutagen

%description
Oggify provides the tools needed to convert an
audio library from one format to another. Originally designed to handle
the author's need of FLAC to Ogg Vorbis. It ships with support for FLAC
as the source format, and Ogg Vorbis and MP3 as output formats. Requires
FLAC, Vorbis-tools and LAME for full operation. Supports other formats
through a plugin system.

%prep
chmod 644 %{SOURCE0}
chmod 644 %{SOURCE1}
%setup -qn %{project}-%{commit0} -a 1
rm tag_wrapper --recursive
mv tag_wrapper-%{commit1} tag_wrapper

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%files -f INSTALLED_FILES
%license COPYING
%dir %{python2_sitelib}/%{name}
%dir %{python2_sitelib}/%{name}/plugins
%dir %{python2_sitelib}/tag_wrapper

%changelog
* Mon Jun 15 2015 Gerald Cox <gbcox@fedoraproject.org> - 2.0.7-5.20150615git4412e37
- Merge tag_wrapper within SPEC rhbz#1175023 comment 29.

* Fri Jun 12 2015 Gerald Cox <gbcox@fedoraproject.org> - 2.0.7-4
- Implement recommendations from rhbz#1175023 comment 19.

* Wed Dec 17 2014 Gerald Cox <gbcox@fedoraproject.org> - 2.0.7-3
- Changed to github commit specification to resolve rhbz#1175023

* Thu Dec 11 2014 Gerald Cox <gbcox@fedoraproject.org> - 2.0.7-2
- Changed SPEC to point back to SPR version

* Sat Dec 06 2014 Gerald Cox <gbcox@fedoraproject.org> - 2.0.7-1
- Initial Build 2.0.7-1
