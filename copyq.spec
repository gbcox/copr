%global project		CopyQ
Name:		copyq	
Summary:	Advanced clipboard manager
Version:	2.4.9
Release:	1%{?dist}
License:	GPLv3+
Url:		https://hluk.github.io/%{project}/
Source0:	https://github.com/hluk/%{project}/archive/v%{version}.tar.gz#/%{project}-%{version}.tar.gz
BuildRequires:	libXtst-devel, libXfixes-devel, desktop-file-utils
BuildRequires:	kf5-rpm-macros, qt5-qtbase-devel, qt5-qtwebkit-devel, qt5-qtsvg-devel
BuildRequires:	qt5-qttools-devel, qt5-qtscript-devel, qwt-qt5-devel
BuildRequires:	extra-cmake-modules, appstream-qt-devel, libappstream-glib

%description
CopyQ is advanced clipboard manager with searchable and editable history with
support for image formats, command line control and more.

%prep
chmod 644 %{SOURCE0}
%setup -qn %{project}-%{version}
sed -i '/DQT_RESTRICTED_CAST_FROM_ASCII/d' CMakeLists.txt

%build
%{cmake_kf5}	-DCMAKE_SKIP_RPATH=TRUE \
				-DWITH_QT5=ON \
				-DWITH_TESTS=TRUE \
				-DCMAKE_INSTALL_PREFIX=%{_prefix} \
				-DPLUGIN_INSTALL_PREFIX=%{_libdir}/%{name}/plugins \
				-DTRANSLATION_INSTALL_PREFIX:PATH=share/%{name}/locale
make %{?_smp_mflags}

%install
make DESTDIR=$RPM_BUILD_ROOT install
%find_lang %{name} --with-qt

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop
appstream-util validate-relax --nonet $RPM_BUILD_ROOT%{_datadir}/appdata/%{name}.appdata.xml

%files -f %{name}.lang
%doc AUTHORS CHANGES HACKING README.md
%license LICENSE
%{_bindir}/%{name}
%{_libdir}/%{name}/
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*.png
%{_datadir}/icons/hicolor/*/apps/%{name}*.svg
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/locale/
%{_datadir}/%{name}/themes/

%post
/usr/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /usr/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%changelog
* Sun Aug 30 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.9-1
- Upstream release rhbz#1258225

* Tue Jul 7 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.8-2
- Upstream release rhbz#1240642

* Tue Jul 7 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.8-1
- Upstream release rhbz#1240642

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Jun 1 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.7-1
- Upstream release rhbz#1227038

* Fri May 8 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-6
- Change requirement from Qt4 to Qt5

* Fri May 1 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-5
- Deactivate DEBUG messages rhbz#1217874

* Sat Apr 25 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-4
- Remove superfluous explicit requires rhbz#1211831

* Fri Apr 24 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-3
- Resolve duplicate file warnings, runtime dependencies rhbz#1211831

* Fri Apr 24 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-2
- Enable end user testing - http://goo.gl/ue6e2F rhbz#1211831

* Tue Apr 14 2015 Gerald Cox <gbcox@fedoraproject.org> 2.4.6-1
- Initial Build 2.4.6-1 rhbz#1211831
