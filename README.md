Forefront - Fedora COPR GIT Repository
==================================

This repository contains RPM SPEC files and other utilities for maintaining
the Forefront - Fedora COPR Repository.

Additional information can be found in the repository wiki:
https://bitbucket.org/gbcox/copr/wiki/Home

Licensed under GNU GPL version 3

Copyright © 2015 Gerald Cox (unless explicitly stated otherwise)