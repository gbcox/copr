Name:			megatools
Version:		1.9.95
Release:		4%{?dist}
Summary:		Command line client for MEGA
License:		GPLv3+
URL:			http://megatools.megous.com/
Source0:		http://megatools.megous.com/builds/%{name}-%{version}.tar.gz
Source1:		http://megatools.megous.com/builds/%{name}-%{version}.tar.gz.asc
BuildRequires:	fuse-devel, libcurl-devel, openssl-devel, glib2-devel, gmp-devel
BuildRequires:	gobject-introspection-devel

%description
Megatools is a collection of programs for accessing Mega service from a command
line of your desktop or server.

Megatools allow you to copy individual files as well as entire directory trees
to and from the cloud. You can also perform streaming downloads for example to
preview videos and audio files, without needing to download the entire file.

You can register account using a "megareg" tool, with the benefit of having
true control of your encryption keys.

Megatools are robust and optimized for fast operation - as fast as Mega servers
allow. Memory requirements and CPU utilization are kept at minimum.

%package		devel
Summary:		Include files and mandatory libraries for development megatools
Requires:		%{name}%{?_isa} = %{version}-%{release}

%description	devel
Include files and mandatory libraries for development.

%prep
%setup -q %{name}-%{version}
autoreconf -fi


%build
%configure	--disable-silent-rules \
		--disable-static
sed -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
	-e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
	-e 's|CC="\(g.*\)"|CC="\1 -Wl,--as-needed"|' \
	-i libtool
sed -i 's/\(GLIB_CFLAGS = \)-pthread/\1/' Makefile
export LD_LIBRARY_PATH=$PWD/.libs
make %{?_smp_mflags}

%install
make DESTDIR=$RPM_BUILD_ROOT install
find $RPM_BUILD_ROOT -name "*" -exec chrpath --delete {} \; 2>/dev/null
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
find $RPM_BUILD_ROOT -name '*.a' -exec rm -f {} ';'
find $RPM_BUILD_ROOT -size "0" -exec rm -f {} ';'
rm $RPM_BUILD_ROOT%{_docdir}/%{name}/INSTALL
ln -s libtools/sjson.gen.c

%check
make check

%post -n %{name} -p /sbin/ldconfig

%postun -n %{name} -p /sbin/ldconfig

%files
%doc HACKING NEWS README
%license LICENSE
%{_bindir}/mega*
%{_libdir}/libmega.so.*
%dir %{_libdir}/girepository-1.0/
%{_libdir}/girepository-1.0/Mega-1.0.typelib
%dir %{_datadir}/gir-1.0/
%{_datadir}/gir-1.0/Mega-1.0.gir
%{_mandir}/man1/mega*.1.gz
%{_mandir}/man5/megarc.5.gz
%{_mandir}/man7/%{name}.7.gz

%files devel
%{_includedir}/mega/
%{_libdir}/libmega.so
%{_libdir}/pkgconfig/libmega.pc

%changelog
* Mon Jun 15 2015 Gerald Cox <gbcox@fedoraproject.org> - 1.9.95-4
- Implement recommendations from rhbz#1228924 comment 8.

* Mon Jun 15 2015 Gerald Cox <gbcox@fedoraproject.org> - 1.9.95-3
- Implement recommendations from rhbz#1228924 comment 6.

* Fri Jun 12 2015 Gerald Cox <gbcox@fedoraproject.org> - 1.9.95-2
- Implement recommendations from rhbz#1228924 comment 2.

* Tue May 26 2015 Gerald Cox <gbcox@fedoraproject.org> - 1.9.95-1
- Initial release
